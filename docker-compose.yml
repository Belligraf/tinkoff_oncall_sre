version: '3.8'

#####################################################################
# OnCall + MySQL -> Filebeat -> Logstash -> Elasticsearch -> Kibana #
#####################################################################

services:
  oncall-web:
    build: .
    hostname: oncall
    deploy:
      resources:
        limits:
          memory: 2g
    ports:
      - "8080-8082:8080"
    labels:
      co.elastic.logs/enabled: true
    environment:
      - DOCKER_DB_BOOTSTRAP=1
      - IRIS_API_HOST=iris
    volumes:
      - logs_oncall:/home/oncall/var/log/uwsgi
      - ./configs/config.docker.yaml:/home/oncall/config/config.yaml
      - textfile:/var/lib/node_exporter/textfile_collector
    networks:
      - iris

  oncall-mysql:
    hostname: oncall-mysql
    image: mysql:5.7
#    image: arm64v8/mysql:oracle
#    command: --default-authentication-plugin=mysql_native_password
#    restart: always
    deploy:
      resources:
        limits:
          memory: 1g
    labels:
      co.elastic.logs/enabled: true
    environment:
      - MYSQL_ROOT_PASSWORD=1234
    networks:
      - iris

  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    deploy:
      resources:
        limits:
          memory: 256m
    ports:
      - "9090:9090"
    labels:
      co.elastic.logs/enabled: false
    command:
      - --config.file=/etc/prometheus/prometheus.yml
    volumes:
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml:ro
      - ./prometheus/alert.rules:/etc/prometheus/alert.rules
    depends_on:
      - oncall-web
    networks:
      - iris

  node-exporter:
    image: quay.io/prometheus/node-exporter:latest
    container_name: node-exporter
    deploy:
      resources:
        limits:
          memory: 256m
    labels:
      co.elastic.logs/enabled: false
    command:
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)'
#     Предоставляет статистику об устройствах в /proc/mdstat (ничего не делает, если /proc/mdstat отсутствует).
      - '--no-collector.mdadm'
#     Предоставляет сетевую статистику, специфичную для конфигураций InfiniBand и Intel Omni Path.
      - '--no-collector.infiniband'
#     Предоставляет статистику электропитания из /sys/class/power_supply
      - '--no-collector.powersupplyclass'
      - '--collector.textfile.directory=/var/lib/node_exporter/textfile_collector'
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
      - textfile:/var/lib/node_exporter/textfile_collector
    restart: unless-stopped
    ports:
      - "9100:9100"
    networks:
      - iris
    depends_on:
      - oncall-web

  blackbox_exporter:
    image: prom/blackbox-exporter
    container_name: backbox_exporter
    deploy:
      resources:
        limits:
          memory: 256m
    labels:
      co.elastic.logs/enabled: false
    volumes:
      - ./prometheus/blackbox.yml:/config/blackbox.yml
    command:
      - '--config.file=/config/blackbox.yml'
    restart: unless-stopped
    networks:
      - iris
    ports:
      - '9115:9115'
    depends_on:
      - oncall-web

  business_metric:
    container_name: buisness_metric
    build:
      context: business_metric
      dockerfile: Dockerfile
    depends_on:
        - oncall-web
        - oncall-mysql
    ports:
      - 8070:8070
    networks:
      - iris


  setup:
   image: elastic/elasticsearch:${STACK_VERSION}
   labels:
     co.elastic.logs/enabled: false
   volumes:
     - certs:/usr/share/elasticsearch/config/certs
   user: "0"
   command: >
     bash -c '
       if [ x${ELASTIC_PASSWORD} == x ]; then
         echo "Set the ELASTIC_PASSWORD environment variable in the .env file";
         exit 1;
       elif [ x${KIBANA_PASSWORD} == x ]; then
         echo "Set the KIBANA_PASSWORD environment variable in the .env file";
         exit 1;
       fi;
       if [ ! -f config/certs/ca.zip ]; then
         echo "Creating CA";
         bin/elasticsearch-certutil ca --silent --pem -out config/certs/ca.zip;
         unzip config/certs/ca.zip -d config/certs;
       fi;
       if [ ! -f config/certs/certs.zip ]; then
         echo "Creating certs";
         echo -ne \
         "instances:\n"\
         "  - name: es01\n"\
         "    dns:\n"\
         "      - es01\n"\
         "      - localhost\n"\
         "    ip:\n"\
         "      - 127.0.0.1\n"\
         "  - name: kibana\n"\
         "    dns:\n"\
         "      - kibana\n"\
         "      - localhost\n"\
         "    ip:\n"\
         "      - 127.0.0.1\n"\
         > config/certs/instances.yml;
         bin/elasticsearch-certutil cert --silent --pem -out config/certs/certs.zip --in config/certs/instances.yml --ca-cert config/certs/ca/ca.crt --ca-key config/certs/ca/ca.key;
         unzip config/certs/certs.zip -d config/certs;
       fi;
       echo "Setting file permissions"
       chown -R root:root config/certs;
       find . -type d -exec chmod 750 \{\} \;;
       find . -type f -exec chmod 640 \{\} \;;
       echo "Waiting for Elasticsearch availability";
       until curl -s --cacert /usr/share/elasticsearch/config/certs/ca/ca.crt https://es01:9200 | grep -q "missing authentication credentials"; do sleep 3; done;
       echo "Setting kibana_system password";
       until curl -s -X POST --cacert /usr/share/elasticsearch/config/certs/ca/ca.crt -u "elastic:${ELASTIC_PASSWORD}" -H "Content-Type: application/json" https://es01:9200/_security/user/kibana_system/_password -d "{\"password\":\"${KIBANA_PASSWORD}\"}" | grep -q "^{}"; do sleep 10; done;
       echo "All done!";
     '
   healthcheck:
     test: ["CMD-SHELL", "[ -f config/certs/es01/es01.crt ]"]
     interval: 1s
     timeout: 5s
     retries: 120
   networks:
     - iris

  es01:
    depends_on:
      setup:
        condition: service_healthy
    image: elastic/elasticsearch:${STACK_VERSION}
    labels:
      co.elastic.logs/enabled: false
    volumes:
      - certs:/usr/share/elasticsearch/config/certs
      - esdata01:/usr/share/elasticsearch/data
    ports:
      - ${ES_PORT}:9200
    environment:
      - node.name=es01
      - cluster.name=${CLUSTER_NAME}
      - discovery.type=single-node
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - bootstrap.memory_lock=true
      - xpack.security.enabled=true
      - xpack.security.http.ssl.enabled=true
      - xpack.security.http.ssl.key=certs/es01/es01.key
      - xpack.security.http.ssl.certificate=certs/es01/es01.crt
      - xpack.security.http.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.enabled=true
      - xpack.security.transport.ssl.key=certs/es01/es01.key
      - xpack.security.transport.ssl.certificate=certs/es01/es01.crt
      - xpack.security.transport.ssl.certificate_authorities=certs/ca/ca.crt
      - xpack.security.transport.ssl.verification_mode=certificate
      - xpack.license.self_generated.type=${LICENSE}
    mem_limit: ${ES_MEM_LIMIT}
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s --cacert /usr/share/elasticsearch/config/certs/ca/ca.crt https://localhost:9200 | grep -q 'missing authentication credentials'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120
    networks:
      - iris


  kibana:
    depends_on:
      es01:
        condition: service_healthy
    image: elastic/kibana:${STACK_VERSION}
    labels:
      co.elastic.logs/enabled: false
    volumes:
      - certs:/usr/share/kibana/config/certs
      - kibanadata:/usr/share/kibana/data
    ports:
      - ${KIBANA_PORT}:5601
    environment:
      - SERVERNAME=kibana
      - ELASTICSEARCH_HOSTS=https://es01:9200
      - ELASTICSEARCH_USERNAME=kibana_system
      - ELASTICSEARCH_PASSWORD=${KIBANA_PASSWORD}
      - ELASTICSEARCH_SSL_CERTIFICATEAUTHORITIES=config/certs/ca/ca.crt
      - XPACK_SECURITY_ENCRYPTIONKEY=${ENCRYPTION_KEY}
      - XPACK_ENCRYPTEDSAVEDOBJECTS_ENCRYPTIONKEY=${ENCRYPTION_KEY}
      - XPACK_REPORTING_ENCRYPTIONKEY=${ENCRYPTION_KEY}
    mem_limit: ${KB_MEM_LIMIT}
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s -I http://localhost:5601 | grep -q 'HTTP/1.1 302 Found'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120
    networks:
      - iris


  filebeat01:
    depends_on:
      es01:
        condition: service_healthy
    image: elastic/filebeat:${STACK_VERSION}
    labels:
      co.elastic.logs/enabled: false
    user: root
    command: /bin/sh -c "filebeat -e -strict.perms=false && apt-get update && apt install telnet"
    volumes:
      - certs:/usr/share/filebeat/certs
      - filebeatdata01:/usr/share/filebeat/data
      - logs_oncall:/usr/share/filebeat/ingest_data/
#      - "./filebeat_ingest_data/:/usr/share/filebeat/ingest_data/"
      - "./filebeat.yml:/usr/share/filebeat/filebeat.yml:ro"
      - "/var/lib/docker/containers:/var/lib/docker/containers:ro"
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
    environment:
      - ELASTIC_USER=elastic
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - ELASTIC_HOSTS=https://es01:9200
      - KIBANA_HOSTS=http://kibana:5601
      - LOGSTASH_HOSTS=http://logstash01:9600
    mem_limit: ${FB_MEM_LIMIT}
    networks:
      - iris

  logstash01:
    depends_on:
      es01:
        condition: service_healthy
      kibana:
        condition: service_healthy
    image: elastic/logstash:${STACK_VERSION}
    labels:
      co.elastic.logs/enabled: false
    user: root
    volumes:
      - certs:/usr/share/logstash/certs
      - logstashdata01:/usr/share/logstash/data
      - "./logstash_ingest_data/:/usr/share/logstash/ingest_data/"
      - "./logstash.conf:/usr/share/logstash/pipeline/logstash.conf:ro"
    environment:
      - xpack.monitoring.enabled=false
      - ELASTIC_USER=elastic
      - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
      - ELASTIC_HOSTS=https://es01:9200
    mem_limit: ${LS_MEM_LIMIT}
    networks:
      - iris

  grafana:
    image: grafana/grafana:10.2.0
    ports:
      - 3000:3000
    restart: unless-stopped
    volumes:
      - ./grafana/provisioning/datasources:/etc/grafana/provisioning/datasources
      - grafana-data:/var/lib/grafana
    networks:
      - iris

  alertmanager:
    container_name: alertmanager
    image: prom/alertmanager:v0.26.0
    container_name: alertmanager
    ports:
      - "9093:9093"
    volumes:
      - ./alertmanager:/etc/alertmanager
    command:
      - '--config.file=/etc/alertmanager/alertmanager.yml'
    networks:
      - iris

  prober:
    container_name: prober
    depends_on:
      - oncall-web
    ports:
      - 8092:8092
    environment:
      - ONCALL_EXPORTER_API_URL=http://oncall:8080
    build:
      context: ./prober
      dockerfile: Dockerfile
    restart: unless-stopped
    networks:
      - iris

  sla:
    container_name: sla
    build:
      context: ./sla
      dockerfile: ./Dockerfile
    labels:
      co.elastic.logs/enabled: false
    depends_on:
      - oncall-mysql
    networks:
      - iris

  nginx:
    container_name: nginx
    build:
      context: ./nginx
      dockerfile: ./Dockerfile
    depends_on:
      - oncall-web
    ports:
      - "5100:5100"
    restart: always
    networks:
      - iris

  nginx_exporter:
    container_name: nginx_exporter
    image: nginx/nginx-prometheus-exporter:1.0.0
    command:
      - --nginx.scrape-uri=http://nginx:5100/metrics
    ports:
      - "9113:9113"
    networks:
      - iris

networks:
  iris:
  default:
    external: false

volumes:
  textfile:
  logs_oncall:
  certs:
    driver: local
  esdata01:
    driver: local
  kibanadata:
    driver: local
  filebeatdata01:
    driver: local
  logstashdata01:
    driver: local
  grafana-data:
