# Tinkoff SRE course

original GitHub project: https://github.com/linkedin/oncall/

Playlist: https://www.youtube.com/playlist?list=PLG2E3ZauGmcww4-YkDjcOEtdFlWFquVkd

what was added:
* Prometheus (node/blackbox/nginx exporter)
* Grafana
* OnCall + MySQL -> Filebeat -> Logstash -> Elasticsearch -> Kibana
* Alertmanager for telegram
* Prober
* nginx for load balancer 
* seamless update

Ports:
- 8080-8082 oncall application
- 9090 prometheus
- 8092 Prober
- 9100 node-exporter
- 9115 blackbox exporter
- 3000 Grafana
- 5100 nginx
- 9113 nginx exporter 
