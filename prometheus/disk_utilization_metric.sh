#!/bin/bash

DISK="sda"
metric_name="disk_utilization_percentage"
metrics_directory="/var/lib/node_exporter/textfile_collector"

mkdir -p "$metrics_directory"

if which iostat &> /dev/null; then
  DISK_UTILIZATION=$(iostat -dx | grep "$DISK" | awk '{print $12}')

  echo "# HELP $metric_name Utilization percentage of the disk"
  echo "# TYPE $metric_name gauge"
  echo "$metric_name $DISK_UTILIZATION" > "$metrics_directory/$metric_name.prom"
else
  echo "iostat command is not available. Please install sysstat package."
  exit 1
fi
