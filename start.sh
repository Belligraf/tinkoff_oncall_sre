#!/bin/bash

# Запустим cron в фоне
cron &

# Запустим основной процесс
sudo -EHu oncall bash -c "source /home/oncall/env/bin/activate && python -u /home/oncall/entrypoint.py"
