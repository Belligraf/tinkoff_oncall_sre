#!/bin/bash

# Set the name of your Docker Compose project
CONTAINER_NAME="tinkoff_oncall_sre_oncall-web_"

# Number of containers
TOTAL_CONTAINERS=3

docker-compose build oncall-web

# Loop through containers and recreate them gradually
for ((i=1; i<=$TOTAL_CONTAINERS; i++))
do
  echo "***	Stop old container	***"
  docker stop $CONTAINER_NAME$i
  echo "***	Remove old containr	***"
  docker rm $CONTAINER_NAME$i
  echo "***	Create new container	***"
  docker-compose run --name $CONTAINER_NAME$i -d oncall-web
  echo "***	Finish $CONTAINER_NAME$i	***"
  sleep 15
done

echo "Done."
