import time
import json
import hmac
import base64
import hashlib
import requests

from prometheus_client import Gauge, start_http_server

# in oncall
# docker ps
# sudo docker exec -it {db_container_id} /bin/bash
# mysql -u root -p
# INSERT INTO oncall.application (id,name,`key`) VALUES (1,'test_app','test_key');

KEY = 'test_key'
APPLICATION = 'test_app'
ADDRESS = 'http://oncall:8080'
TWO_WEEK = 607_800 * 2


def sign_request(method, path, body):
    window = int(time.time()) // 5
    text = '%s %s %s %s' % (window, method, path, body)
    hashed = hmac.new(KEY.encode(), text.encode('utf-8'), hashlib.sha512)
    return base64.urlsafe_b64encode(hashed.digest()).decode('utf-8')


def send_data(method='POST'):
    def inner(func):
        def wrapper(*args, **kwargs):
            url, data = func(*args, **kwargs)
            url = ADDRESS + url
            signature = sign_request(
                method, '/' + url.split('/', maxsplit=3)[-1], json.dumps(data)
            )
            response = session.request(
                method,
                url,
                headers={
                    'AUTHORIZATION': f'hmac {APPLICATION}:{signature}'
                },
                json=data,
            )
            return response.json()

        return wrapper

    return inner


@send_data(method='GET')
def get_teams():
    url = f'/api/v0/teams'
    data = {}
    return url, data


@send_data(method='GET')
def get_users_in_team(team: str):
    url = f'/api/v0/teams/{team}/users'
    data = {}
    return url, data


@send_data(method='GET')
def get_active_event_for_team(team, role='primary'):
    url = f'/api/v0/teams/{team}/oncall/{role}'
    data = {}
    return url, data


@send_data(method='GET')
def get_events(team_name, start_gt, end_lt):
    url = f'/api/v0/events?team={team_name}&start__gt={start_gt}&end__lt=={end_lt}&role=primary'
    data = {}
    return url, data


def get_number_users_in_teams(teams):
    count_users_in_team = {}

    for team in teams:
        team = str(team)
        count_users_in_team[team] = len(get_users_in_team(team))

    return count_users_in_team


def get_number_days_for_schedule_ready(teams):
    count_days_in_schedule_team = {}
    today = time.time()
    end = today + TWO_WEEK
    for team in teams:
        count_days_in_schedule_team[team] = len(get_events(team, today, end))

    return count_days_in_schedule_team


def main():
    teams = get_teams()
    count_users_in_team = get_number_users_in_teams(teams)
    count_days_in_schedule_team = get_number_days_for_schedule_ready(teams)

    for team in teams:
        gauge_number_users_in_teams.labels(team).set(count_users_in_team[team])
        gauge_number_days_for_schedule_ready.labels(team).set(count_days_in_schedule_team[team])


if __name__ == '__main__':
    start_http_server(8070)
    session = requests.session()

    gauge_number_users_in_teams = Gauge('number_users_in_teams',
                                        'counts the number of people in the team',
                                        ['team'])
    gauge_number_days_for_schedule_ready = Gauge('number_days_for_schedule_ready',
                                                 'counts how many days each of the teams has a schedule',
                                                 ['team'])

    while True:
        try:
            main()
        except requests.exceptions.ConnectionError:
            print('Connection Error')
        time.sleep(15)
